# Array

This project counts amount of proper parenthesis in Array. By proper amount of parenthesis we understand the case when for the each

opening parentheses we have appropriated closing one.

for example you have this Array:

```
#!javascript

[

 '() [] () ([]()[])',

 '( (] ([)]',

 '{}[]()',

 '{{}]}',

 '{[}',

 '[}]'

];
```

and the output should be like this:


```
#!javascript
[

  'YES',

  'NO',

  'YES'

  'NO'

  'NO',

  'NO'

]
```

# How to use it? #

##Clone
```bash
$ cd /tmp
$ git clone https://bitbucket.org/husseinbashiri/array.git
$ cd array
```

##Installation

requirement is nodejs, npm and gulp. for more information about packages please see [package.json](https://bitbucket.org/husseinbashiri/array/raw/0893d0b1d3c5dc8fbd433a44167529ed8cee94b3/package.json)

```
#!bash
 npm install
```

## run project server ##


```
#!bash

 gulp
```
serving project is the default task in gulp.
