class Array {
  /**
   * @constructor  initial process
   * @param  string  input  string array
   * @return  string  output string
   */
   constructor(input) {
     this.array = this.convertStringToArray(input);
   }

 /**
  * process function handle the whole operation duty
  * @return array            
  */
  process() {
    let output = [];
    for(let item of this.array) {
      output.push(this.checkForProperParentheses(item));
    }
    return output;
  }

/**
 * checkForProperParentheses function check string for ProperParentheses
 * @param  string strItem 
 * @return string         
 */
 checkForProperParentheses(strItem) {
  let stack = [];
  let openP = "(";
  let closeP = ")";
  let hasP = false;
  
  for(let item of strItem) {
     
     if (item == openP) {
      hasP = true;
      stack.push(item);
     }
     if (item == closeP) {
      if(stack.length > 0) {
        stack.pop();
      } else {
        return "NO";
      }
     }
  }
   if (hasP && stack.length == 0) {
      return "YES";
    } else {
      return "NO";
    }
}

/**
 * convertStringToArray function convert string function to array
 * @param  string strInput should use proper format of string
 * @return array          
 */
 convertStringToArray(strInput) {
  try {
    strInput = strInput.replace(/'/g, '"');
    return JSON.parse(strInput);
  } catch(e) {
    alert('Your Array is not valid. please check it and try again. thank you.');
    return [];
  }
}
}