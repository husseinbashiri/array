const gulp = require('gulp');
const babel = require('gulp-babel');
const webserver = require('gulp-webserver');
const uglify = require('gulp-uglify');
const pump = require('pump');
const runSequence = require('run-sequence');

gulp.task('babel', (cb) => {
  pump([
        gulp.src('src/js/*.js'),
        babel({
           presets: ['es2015']
        }),
        uglify(),
        gulp.dest('dist')
    ],
    cb
  );
});

gulp.task('html_handler', () => {
  return gulp.src('src/*.html')
  .pipe(gulp.dest('dist'));
});

gulp.task('webserver', () => {
  gulp.src('dist')
  .pipe(webserver({
    livereload: true,
    open: true,
  }));
});

gulp.task('build', (callback) => {
  runSequence(
    ['babel', 'html_handler'],
    callback
    )
});

gulp.task('watch', () => {
 gulp.watch('src/js/*.js', ['babel']);
 gulp.watch('src/*.html', ['html_handler']);
});

gulp.task('default', (callback) => {
  runSequence(
    ['build', 'watch'],
    ['webserver'],
    callback
    )
});